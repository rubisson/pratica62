
import utfpr.ct.dainf.if62c.pratica.Jogador;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica62 {
    public static void main(String[] args) {
        Jogador j1 = new Jogador(10, "João");
        Jogador j2 = new Jogador(2, "Maria");

        
        System.out.println(j1.compareTo(j2));
    }
}
